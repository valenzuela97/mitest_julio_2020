- Explicar conceptualmente que es una llamada AJAX. Efectúe un diagrama que indique la interacción entre una aplicación cliente servidor usando AJAX de por medio.

R: Una llamada AJAX es una petición y respuesta asíncrona por HTTP entre un cliente y un servidor web, desde el lado del cliente se suele seleccionar una sección específica del DOM (Document Object Model) para que al momento de realizar la comunicación asíncrona (realizadas en segundo plano) con el servidor se recargue solamente esa sección del documento. Cabe mencionar que el formato utilizado para el intercambio de información son principalmente XML o JSON.
*Adjunto imagen del diagrama.

- Explicar qué es la arquitectura orientada a servicios REST. Diagrame indicando como funciona en comparación a un servicio web tradicional.

R: La arquitectura REST permite separar el cliente del servidor, al ser independientes, cuando finaliza una petición, la información no se almacena del lado del servidor (“stateless”) por consiguiente, existe una interfaz de comunicación estándar (con sus permisos de acceso correspondientes)  que es intermediaria entre las componentes anteriormente mencionadas. Cabe mencionar que se puede configurar el almacenamiento en caché que posee la interfaz de comunicación para optimizar aún más la comunicación cliente-servidor.
*Adjunto imagen del diagrama.

- ¿Que clase de códigos HTTP existen? 

R:
1XX: Respuestas Informativas
2XX: Respuestas satisfactorias
3XX: Redirecciones
4XX: Errores del Cliente
5XX: Errores del Servidor

- Si tuviera que generar un código que efectúe la creación de un documento, la eliminación de un registro y el acceso denegado a cierto documento. ¿Que codigo utilizaria en estos 3 casos?

R:
Creación de un documento: 201
Eliminación de un Registro: 200
Acceso denegado: 403

- Cómo funciona un repositorio versionador de código. En este contexto, ¿Que es GIT?

R: GIT es un sistema de control de versiones, que permite controlar y gestionar las modificaciones realizadas a un proyecto almacenado en un repositorio de código, lo que facilita el trabajo en equipo entre los programadores de un proyecto. La principal característica de GIT es que puede subdividir el desarrollo del proyecto en ramas; a partir de la rama principal, se pueden crear ramas de desarrollador en donde el programador puede tener un ambiente de desarrollo aislado para trabajar, que no comprometa la integridad del proyecto que se encuentra en la rama principal. Una vez culminado el desarrollo en alguna de las ramas secundarias, esta rama se puede integrar (si fuera necesario) a la rama principal y tener una nueva versión del software.  

- Explique que es la metodología Gitflow

R: Es un flujo de trabajo basado en GIT, en el cual, como mínimo deben existir 2 ramas principales para el desarrollo del proyecto, la rama Master (contiene las versiones estables del proyecto) y la rama Develop (para el desarrollo). Además deben existir ramas de apoyo que faciliten el desarrollo en paralelo, las cuales son: las ramas Feature (surgen de la rama develop, y almacenan código con nuevas características, generalmente son repositorios locales), las ramas Release (surgen de la rama develop, contienen código de la versión que se va a liberar próximamente, al finalizar la rama, el código pasará a la rama develop y master) y las ramas Hotfix (surgen de la rama master y contienen código con errores que se necesitan arreglar urgentemente).

- Explicar brevemente en qué consisten las metodologías ágiles y conceptualice una de ellas.

R: Son metodologías de gestión de proyectos flexibles, en donde la fase de planificación no es tan estricta y  el equipo de trabajo se debe adaptar constantemente a las exigencias del mercado y los clientes. Esto permite una mayor rapidez en la entrega de los proyectos, fomentar la proactividad entre los miembros del equipo y posiblemente una mayor calidad en el producto final debido a la constante muestra de entregables con el  cliente durante el desarrollo. 
Una de estas metodologías ágiles es SCRUM, En esta metodología existen 3 roles dentro del equipo, el Product Owner (quién decide qué tareas se van a realizar, y cuáles se van realizar primero que otras), el Scrum Master (quien supervisa que se esté aplicando correctamente la metodología SCRUM) y el Equipo de desarrollo. la gestión con SCRUM se inicia a partir del Product Backlog o Pila de trabajo (el cual es una lista con todos los requerimientos del cliente y es gestionado por el Product Owner), posteriormente se descompone la pila de trabajo y se trabajan esos requerimientos con Sprints o iteraciones (por parte del equipo de desarrollo) en cortos periodos de tiempo, al culminar un sprint, se obtendrá un entregable funcional que puede ser mostrado al cliente.


