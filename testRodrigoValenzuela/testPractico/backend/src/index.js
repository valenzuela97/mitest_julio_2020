"use strict";

const express = require("express");
const app = express();
const morgan = require("morgan");

// SERVER CONFIG
app.set("port", process.env.port || 4000);

// MIDDLEWARES
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
//app.set('json spaces', 2);

// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

// RUTAS
app.use(require("./routes/cerveza"));

//ERRORES
app.use((req, res, next) => {
  const err = new Error("not found");
  err.status = 404;
  next(err);
});

app.use((err,req, res, next) => {
  res.status(err.status || 500)
  res.send({
	error:{
		status: err.status || 500,
		message: err.message		
	}
  });
});


// LEVANTAR SERVER
app.listen(app.get("port"), () => {
  console.log(`Server on port ${app.get("port")}`);
});
