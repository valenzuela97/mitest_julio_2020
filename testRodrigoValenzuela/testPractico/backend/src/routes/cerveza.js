const { Router } = require("express");
const router = Router();
const fetch = require("node-fetch");

router.get("/estilos", (req, res,next) => {
  var url =
    "https://sandbox-api.brewerydb.com/v2/styles?key=cbbf1178f826a81a6c14c5665b14320d";
  fetch(url)
    .then((response) => response.json())
    .then((estilos) => res.status(200).json(estilos.data))
    .catch((err) => console.log("error " + err));
});

router.get("/estilo/:id", (req, res,next) => {
  var url =
    "https://sandbox-api.brewerydb.com/v2/styles?key=cbbf1178f826a81a6c14c5665b14320d";
  fetch(url)
    .then((response) => response.json())
    .then((estilo) => {
      const { id } = req.params;
      indice = id - 1;
      if (indice >= 0 && indice <= 175) {
        res.json(estilo.data[indice]);
      } else {
        res.send({ status: "error", message: "id invalida" });
      }
    })
    .catch((err) => console.log("error " + err));
});

router.get("/categorias",(req,res,next)=>{
    var url =
    "https://sandbox-api.brewerydb.com/v2/categories?key=cbbf1178f826a81a6c14c5665b14320d";
  fetch(url)
    .then((response) => response.json())
    .then((categorias) => res.status(200).json(categorias.data))
    .catch((err) => console.log("error " + err));   
});



module.exports = router;
