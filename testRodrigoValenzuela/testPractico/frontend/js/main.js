"use strict";

document.addEventListener("DOMContentLoaded", function (event) {
  const leerCategorias = () => {
    fetch("http://localhost:4000/categorias")
      .then((response) => response.json())
      .then((categorias) => {
        const menuCat = document.getElementById("menuCat");
        const fragment = document.createDocumentFragment();

        for (const cat of categorias) {
          if (cat.id <= 14) {
            const catItem = document.createElement("OPTION");
            catItem.setAttribute("value", cat.id);
            catItem.textContent = cat.name;
            fragment.appendChild(catItem);
          }
          menuCat.appendChild(fragment);
        }
      })
      .catch((err) => console.log(err));
  };

  leerCategorias();

  var selectCat = document.getElementById("menuCat");

  selectCat.addEventListener("change", () => {
    var selectedOption = selectCat.options[selectCat.selectedIndex];
    var selectedOptionInt = parseInt(selectedOption.value);

    const content = document.getElementById("content");

    content.innerHTML = "";
    //console.log(selectedOptionInt);

    fetch("http://localhost:4000/estilos")
      .then((response) => response.json())
      .then((estilos) => {
        for (const estilo of estilos) {
          if (estilo.categoryId === selectedOptionInt) {
            content.innerHTML += `
                     <div>
                        <div> <h1>${estilo.shortName}</h1> </div>
                        
                                <ul> 
                                    <li><span>Nombre Completo:</span> ${estilo.name} </li>
                                    <li><span>Categoria:</span> ${estilo.category.name} </li>
                                    <li><span>Descripción:</span> ${estilo.description}</li>
                                    <li><span>IBU Minimo:</span> ${estilo.ibuMin}</li>
                                    <li><span>IBU Máximo:</span> ${estilo.ibuMax}</li>
                                    <li><span>ABV Minimo:</span> ${estilo.abvMin}</li>
                                    <li><span>ABB Máximo:</span> ${estilo.abvMax}</li>
                                    <li><span>SRM Minimo:</span> ${estilo.srmMin}</li>
                                    <li><span>SRM Máximo:</span> ${estilo.srmMax}</li>
                                    <li><span>OG Minimo:</span> ${estilo.ogMin}</li>
                                    <li><span>FG Minimo:</span> ${estilo.fgMin}</li>
                                    <li><span>FG Máximo:</span> ${estilo.fgMax}</li>
                                </ul>
                        
                     </div>                    
                    `;
          }
        }
      })
      .catch((err) => console.log(err));
  });
});
